import java.util.concurrent.TimeUnit;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
public class DesafioTotvs {
    static String url = "http://bassel.brazilsouth.azurecontainer.io/app/#/login";
    static By userName = By.xpath("//input[@type='text']");
    static By password = By.xpath("//input[@type='password']");
    static By botaoLogin = By.xpath("//button[@type='submit']");
    static By leilao = By.linkText("Sign Up");
    
    public static void main(String[] teste) throws Exception {
        
        testDesafioTotvs();
    }
    
    public static void testDesafioTotvs() throws Exception {
    	System.setProperty("webdriver.chrome.driver", "src/main/resources/drivers/chromedriver.exe");
    	WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get(url);
        
        //Acessar com usuario nao cadastrado e senha cadastrada
        driver.findElement(userName).click();
        driver.findElement(userName).clear();
        driver.findElement(userName).sendKeys("said1");
        driver.findElement(password).click();
        driver.findElement(password).clear();
        driver.findElement(password).sendKeys("Trocar@5");
        driver.findElement(botaoLogin).click();
        
        //Acessar com usuario cadastrado e senha nao cadastrada
        driver.findElement(userName).click();
        driver.findElement(userName).clear();
        driver.findElement(userName).sendKeys("said");
        driver.findElement(password).click();
        driver.findElement(password).clear();
        driver.findElement(password).sendKeys("Trocar@");
        driver.findElement(botaoLogin).click();
        
        //Acessar com usuario e senha cadastrada
        driver.findElement(userName).click();
        driver.findElement(userName).clear();
        driver.findElement(userName).sendKeys("said");
        driver.findElement(password).click();
        driver.findElement(password).clear();
        driver.findElement(password).sendKeys("Trocar@5");
        driver.findElement(botaoLogin).click();
        
        //Clicar no leilao test 1
        driver.findElement(By.linkText("Test 1")).click();
        //Clicar no link Auctions
        driver.findElement(By.linkText("Auctions")).click();
        
        //Fazer log out da aplicacao
        driver.findElement(By.linkText("Sign Out")).click();
        
        //Fazer cadastro com CPF INVALIDO
        driver.findElement(leilao).click();
        driver.findElement(By.id("name")).click();
        driver.findElement(By.id("name")).clear();
        driver.findElement(By.id("name")).sendKeys("bassel");
        driver.findElement(By.xpath("//div/div")).click();
        driver.findElement(By.id("username")).click();
        driver.findElement(By.id("username")).clear();
        driver.findElement(By.id("username")).sendKeys("said89");
        driver.findElement(By.id("password")).click();
        driver.findElement(By.id("password")).clear();
        driver.findElement(By.id("password")).sendKeys("123456789");
        driver.findElement(By.id("email")).click();
        driver.findElement(By.id("email")).clear();
        driver.findElement(By.id("email")).sendKeys("teste@gmail.com");
        WebElement cpf = driver.findElement(By.id("cpf"));
        cpf.click();
        cpf.clear();
        cpf.sendKeys("222.222.222-22");
        
        //Fazer cadastro com CPF VALIDO e um dos campos em branco
        driver.findElement(By.id("name")).click();
        driver.findElement(By.id("name")).clear();
        driver.findElement(By.id("username")).click();
        driver.findElement(By.id("username")).clear();
        driver.findElement(By.id("username")).sendKeys("said89");
        driver.findElement(By.id("password")).click();
        driver.findElement(By.id("password")).clear();
        driver.findElement(By.id("password")).sendKeys("123456789");
        driver.findElement(By.id("email")).click();
        driver.findElement(By.id("email")).clear();
        driver.findElement(By.id("email")).sendKeys("teste@gmail.com");
        
        //Verificar mensagem de erro
        String textErro = driver.findElement(By.xpath("/html/body/app-root/app-sing-up/div/div/div/div[2]/div")).getText();
        if (textErro.equals("Login/Password invalid. Try again.")) {
            System.out.println("Validacao com sucesso");
        }
        else {
            System.out.println("Validacao com erro " +textErro);
        }
        driver.quit();
    }


    }

